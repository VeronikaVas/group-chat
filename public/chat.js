var socket = io.connect('http://localhost:8080');
var feedback = $("#feedback")

$("#send-message").click(function (e) {
    e.preventDefault();
    if ($('#txt').val() != '') {
        socket.emit('chat_message', $('#txt').val());
        $('#txt').val('');
    }

})

// append the chat text message
socket.on('chat_message', function (msg) {
    $('#messages').append($('<li>').html(msg));
    feedback.html("");
});

// append text if someone is online
socket.on('is_online', function (username) {
    $('#messages').append($('<li>').html(username));
});

//emit typing
$('#txt').bind("keypress", () => {
    socket.emit('typing')
})

//listen on typing
socket.on('typing', (data) => {
    feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>");
})

// ask username
var username = prompt('Your name:');
socket.emit('username', username);