const express = require('express');
const app = express();

const port = '8080'

//set the template engine pug
app.set('view engine', 'pug')

//middlewares
app.use(express.static('public'))

//routes
app.get('/', (req, res) => {
	res.render('index')
})

//listen on port 8080
const server = app.listen(port, () => {
    console.log('%s App is running at http://localhost:%d in %s mode', '✓', port, app.get('env'));
    console.log('  Press CTRL-C to stop\n');
});

const io = require("socket.io")(server)

io.sockets.on('connection', function(socket) {
    socket.on('username', function(username) {
        socket.username = username;
        io.emit('is_online', '🔵 <i>' + socket.username + ' join the chat</i>');
    });

    socket.on('disconnect', function(username) {
        io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat</i>');
    })

    socket.on('chat_message', function(message) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
    });

    socket.on('typing', (data) => {
    	socket.broadcast.emit('typing', {username : socket.username})
    })

});

